<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Smtp
 */


namespace Swissclinic\ExtendAmasty\Helper;

use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\State;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;

class Data extends \Amasty\Smtp\Helper\Data
{

    public function getCurrentStore()
    {
        $store = $this->storeManager->getStore();

        if ($this->appState->getAreaCode() == (FrontNameResolver::AREA_CODE || 'crontab')) {
            /** @var \Magento\Sales\Model\Order $order */
            if ($order = $this->registry->registry('current_order')) {
                return $order->getStoreId();
            }

            if ($storeId = $this->registry->registry('transportBuilderPluginStoreId')) {
                return $storeId;
            }

            return 0;
        }

        return $store->getId();
    }
}
