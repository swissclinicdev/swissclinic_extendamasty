<?php
namespace Swissclinic\ExtendAmasty\Helper;

class Config extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_GEOIP_REDIRECT_ENABLED = 'swissclinic_extendamasty/redirect/enable';
    const XML_PATH_ORDER_EXPORT_INTEGER= 'swissclinic_extendamasty/orderexport/integer';
}