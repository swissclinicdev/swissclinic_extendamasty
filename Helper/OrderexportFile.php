<?php
namespace Swissclinic\ExtendAmasty\Helper;

use Swissclinic\ExtendAmasty\Helper\Config as ConfigHelper;


class OrderexportFile extends \Amasty\Orderexport\Helper\File
{
    /**
     * Modified parent to include <orderhuvud> tag, and to add increment ID and order row number as xml attributes
     *
     * @param array $fields
     * @param string $xmlOrderTag
     * @param string $xmlOrderItemsTag
     * @param string $xmlOrderItemTag
     * @return bool
     */
    public function writeXML($fields, $xmlOrderTag, $xmlOrderItemsTag, $xmlOrderItemTag)
    {
        $orderItemsXml = [];
        $xmlOpen = '<' . $xmlOrderTag;
        if(isset($fields['ordernr'])) {
            $xmlOpen .= ' ordernr="' . $fields['ordernr'] . '"';
            unset($fields['ordenr']);
        }
        $xmlOpen .= '>';
        $xml = [$xmlOpen];
        $xml[] = '<orderhuvud>';

        $isIntegers = $this->scopeConfig->isSetFlag(ConfigHelper::XML_PATH_ORDER_EXPORT_INTEGER, 'store' );

        $disabledFields = ['fakturaadress_postnr_endast', 'leveransadress_postnr_endast', 'telefon', 'kundnr', 'ordernr', 'betalningsmetod'];

        foreach ($fields as $name => $val) {
            if (!is_array($val)) {
                if( $isIntegers && is_numeric($val) && !in_array($name, $disabledFields)) {
                    $xml[] = '<' . $name . '><![CDATA[' . (float)$val . ']]></' . $name . '>';
                } else {
                    $xml[] = '<' . $name . '><![CDATA[' . $val . ']]></' . $name . '>';
                }
            } else {
                foreach ($val as $orderItemId => $orderItemValue) {
                    if (!array_key_exists($orderItemId, $orderItemsXml)) {
                        $orderItemsXml[$orderItemId] = [];
                    }
                    if( $this->scopeConfig->isSetFlag(ConfigHelper::XML_PATH_ORDER_EXPORT_INTEGER, 'store' ) && is_numeric($orderItemValue)) {
                        $orderItemsXml[$orderItemId][] = '<' . $name . '><![CDATA[' . (float)$orderItemValue . ']]></' . $name . '>';
                    } else {
                        $orderItemsXml[$orderItemId][] = '<' . $name . '><![CDATA[' . $orderItemValue . ']]></' . $name . '>';
                    }
                }
            }
        }

        $xml[] = '</orderhuvud>';
        if (count($orderItemsXml) > 0) {
            $xml[] = '<' . $xmlOrderItemsTag . '>';
            foreach (array_values($orderItemsXml) as $rownum => $orderItemXml) {
                $xml[] = '<' . $xmlOrderItemTag . ' orderradnr="' . ++$rownum . '">';
                $xml[] = implode('', $orderItemXml);
                $xml[] = '</' . $xmlOrderItemTag . '>';
            }
            $xml[] = '</' . $xmlOrderItemsTag . '>';
        }

        $xml[] = '</' . $xmlOrderTag . '>';

        $res = fwrite($this->fileHandler, implode('', $xml));

        return (bool)$res;
    }
}