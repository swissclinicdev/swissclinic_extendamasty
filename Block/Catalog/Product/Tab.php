<?php

namespace Swissclinic\ExtendAmasty\Block\Catalog\Product;

use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Stdlib\ArrayUtils;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\View\AbstractView;
use Amasty\Blog\Helper\Url;

class Tab extends AbstractView
{
    /**
     * @var \Amasty\Blog\Model\ResourceModel\Posts\Collection
     */
    protected $_collection;

    /**
     * @var Url
     */
    protected $_urlHelper;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManagerInterface;

    /**
     * @var Context
     */
    protected $context;

    /**
     * Tab constructor.
     * @param Context $context
     * @param ArrayUtils $arrayUtils
     * @param ObjectManagerInterface $objectManagerInterface
     * @param Url $urlHepler
     */
    public function __construct(
        Context $context,
        ArrayUtils $arrayUtils,
        ObjectManagerInterface $objectManagerInterface,
        Url $urlHepler
    )
    {
        parent::__construct($context, $arrayUtils);
        $this->objectManagerInterface = $objectManagerInterface;
        $this->context = $context;
        $this->_urlHelper = $urlHepler;
    }

    /**
     * @return \Amasty\Blog\Model\ResourceModel\Posts\Collection
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCollection()
    {
        if (!$this->_collection) {
            $collection = $this->objectManagerInterface->create('Amasty\Blog\Model\ResourceModel\Posts\Collection')
                ->addFieldToFilter('post_id', ['in' => $this->getProductBlogIds()])
                ->addFieldToFilter('status', \Amasty\Blog\Model\Source\PostStatus::STATUS_ENABLED)
                ->addStoreFilter($this->context->getStoreManager()->getStore()->getId())
                ->load();

            $this->_collection = $collection;
        }

        return $this->_collection;
    }

    /**
     * @return array
     */
    public function getProductBlogIds()
    {
        return explode(',', $this->getProduct()->getBlogIds());
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function _toHtml()
    {
        if (!count($this->getCollection())) {
            return '';
        }

        $this->setTitle(__('Magazines'));

        return parent::_toHtml();
    }

    /**
     * @param $blogId
     * @return string
     */
    public function getBlogUrl($blogId)
    {
        return $this->_urlHelper->getUrl($blogId);
    }
}
