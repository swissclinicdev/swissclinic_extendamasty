<?php

namespace Swissclinic\ExtendAmasty\Model\Config\Source\Options;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Amasty\Blog\Model\ResourceModel\Posts\CollectionFactory;

class AmastyBlogs extends AbstractSource
{

    /**
     * @var array $_options
     */
    protected $_options;

    /**
     * @var \Amasty\Blog\Model\ResourceModel\Posts\Collection
     */
    protected $_collection;

    /**
     * @var CollectionFactory
     */
    private $_collectionFactory;

    /**
     * AmastyBlogs constructor.
     * @param ObjectManagerInterface $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    )
    {
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * @return array
     */
    public function getAllOptions()
    {
        $result = [
            ['label' => 'Select Blog', 'value' => null]
        ];

        foreach ($this->getCollection() as $index => $item) {
            $result[] = [
                'label' => $item->getTitle() . ' - (ID: ' . $item->getId() . ')',
                'value' => $item->getId()
            ];
        }

        return $result;
    }

    /**
     * @return \Amasty\Blog\Model\ResourceModel\Posts\Collection
     */
    public function getCollection()
    {
        if (!$this->_collection) {
            $this->_collection = $this->_collectionFactory->create();
            $this->_collection
                ->addFieldToSelect('post_id')
                ->addFieldToSelect('title')
            ;
        }
        return $this->_collection;
    }
}