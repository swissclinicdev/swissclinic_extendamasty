<?php

namespace Swissclinic\ExtendAmasty\Plugin;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Swissclinic\ExtendAmasty\Helper\Data;
use Swissclinic\ExtendAmasty\Helper\Config as ConfigHelper;


class Redirect {

    private $cookieInterface;

    private $publicCookieMetadata;

    private $scopeConfig;

    private $helper;

    public function __construct(
        CookieManagerInterface $cookieInterface,
        PublicCookieMetadata $publicCookieMetadata,
        ScopeConfigInterface $scopeConfig,
        Data $helper
    )
    {
        $this->cookieInterface = $cookieInterface;
        $this->publicCookieMetadata = $publicCookieMetadata;
        $this->scopeConfig = $scopeConfig;
        $this->helper = $helper;
    }

    public function beforeSetUrl(\Magento\Backend\Model\View\Result\Redirect $subject, $url) {


        if($this->scopeConfig->isSetFlag(ConfigHelper::XML_PATH_GEOIP_REDIRECT_ENABLED, 'store')) {
            $pos = strpos($url, '?___from_store=');

            if($pos !== false) {
                $cookie = $this->cookieInterface->getCookie('geo_ip_redirect');
                if (!$cookie) {
                    $this->publicCookieMetadata->setDurationOneYear();
                    $this->publicCookieMetadata->setPath('/');
                    $this->cookieInterface->setPublicCookie('geo_ip_redirect', '1', $this->publicCookieMetadata);
                }
            }
        }

        return $url;
    }
}